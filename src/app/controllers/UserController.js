import * as Yup from 'yup';
import User from '../models/User';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required('Name is required'),
      email: Yup.string()
        .email('Valid e-mail is required')
        .required(),
      password: Yup.string()
        .required('Password is required')
        .min(6, 'Minimum 6 characters'),
      confirmPassword: Yup.string().when('password', (passwordField, field) =>
        passwordField
          ? field
              .required('Confirmation is required')
              .oneOf(
                [Yup.ref('password')],
                'New password and confirmation do not match'
              )
          : field
      ),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails!' });
    }

    const userExists = await User.findOne({ where: { email: req.body.email } });

    if (userExists) {
      return res.status(400).json({ error: 'User already exists.' });
    }

    const { id, name, email, provider } = await User.create(req.body);

    return res.json({
      id,
      name,
      email,
      provider,
    });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email('Invalid email'),
      oldPassword: Yup.string().min(6, 'Minimum 6 characters'),
      password: Yup.string().when('oldPassword', (oldPassword, field) =>
        oldPassword
          ? field
              .min(6, 'Minimum 6 characters')
              .required('New password is required')
              .notOneOf(
                [Yup.ref('oldPassword')],
                'New password cannot be the same as old password'
              )
          : field
      ),
      confirmPassword: Yup.string().when('password', (password, field) =>
        password
          ? field
              .required('Confirmation is required')
              .oneOf(
                [Yup.ref('password')],
                'New password and confirmation do not match'
              )
          : field
      ),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails!' });
    }

    const { email, oldPassword } = req.body;

    const user = await User.findByPk(req.userId);

    if (email && email !== user.email) {
      const emailExists = await User.findOne({ where: { email } });

      if (emailExists) {
        return res.status(400).json({ error: 'E-mail already exists.' });
      }
    }

    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'Password does not match!' });
    }

    const { id, name, provider } = await user.update(req.body);

    return res.json({
      id,
      name,
      email,
      provider,
    });
  }
}

export default new UserController();
